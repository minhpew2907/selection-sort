﻿using Stack;

try
{
    StackDemo demo = new StackDemo(3);
    demo.Push(1);
    demo.Push(2);
    demo.Push(3);
    Console.WriteLine("Top: " + demo.Peek());
    Console.WriteLine("Count: " + demo.Count());
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Count: " + demo.Count());
    demo.Push(4);
    demo.Push(5);
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Pop: " + demo.Pop());
    Console.WriteLine("Count: " + demo.Count());

}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
