﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack
{
    public class StackDemo
    {
        public int[] Items;
        public int Top = -1;
        public int MaxStack;

        // Khởi tạo một ngăn xếp với số lượng item
        public StackDemo(int _maxStack)
        {
            MaxStack = _maxStack;
            Items = new int[MaxStack];
            Top = -1;
        }

        // Thêm một phần tử vào ngăn xếp
        public void Push(int item)
        {
            if (Top == MaxStack - 1)
            {
                throw new Exception("Stack is full");
            }
            Top++;
            Items[Top] = item;
        }

        // Lấy và xóa phần tử đầu ngăn xếp
        public int Pop()
        {
            if (Top == -1)
            {
                throw new Exception("Stack is empty");
            }
            int temp = Items[Top];
            Top--;
            return temp;
        }

        // Trả về giá trị của phần tử đầu ngăn xếp mà không xóa nó
        public int Peek()
        {
            if (Top == -1)
            {
                throw new Exception("Stack is empty");
            }
            return Items[Top];
        }

        // Kiểm tra xem ngăn xếp có rỗng hay không
        public bool IsEmpty()
        {
            return Top > -1;
        }

        // Kiểm tra xem ngăn xếp đã đầy hay không
        public bool IsFull()
        {
            return Top == MaxStack - 1;
        }

        // Đếm số lượng phần tử trong ngăn xếp
        public int Count()
        {
            return Top + 1;
        }

    }
}
